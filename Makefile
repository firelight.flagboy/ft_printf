MAKELIB_PATH := makelib

NAME := libftprintf.a

SRCS :=\
	buffer/ft_buffer \
	buffer/ft_buffer_2 \
	buffer/ft_fill_buffer \
	buffer/ft_putbuffer \
	conv/ft_adr \
	conv/ft_badconv \
	conv/ft_bin \
	conv/ft_char \
	conv/ft_color \
	conv/ft_fillstr \
	conv/ft_hex \
	conv/ft_nbr \
	conv/ft_octal \
	conv/ft_unbr \
	fill/ft_backward \
	fill/ft_filldimen \
	fill/ft_fillforward \
	fill/ft_fillforward_annexe \
	flags/ft_get_flags \
	flags/ft_get_flags_2 \
	float/float_utils \
	float/ft_fillforward_float \
	float/ft_float \
	ft_get_int \
	ft_itoa \
	ft_printf \
	ft_singleton \
	ft_sprintf \
	ft_unicode \
	ft_vprintf \
	ft_vsprintf \
	libc/ft_iswhat \
	libc/ft_iswhat_2 \
	libc/ft_str \
	libc/ft_strlen_printf \
	libc/ft_strncpy_printf \

ISLIB := 1
ISDEV := 0

include $(MAKELIB_PATH)/core.mk
